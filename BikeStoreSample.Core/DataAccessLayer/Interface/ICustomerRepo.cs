﻿using BikeStoreSample.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeStoreSample.Core.DataAccessLayer.Interface
{
    public interface ICustomerRepo
    { 
        CustomerModel GetCustomer(int CustomerId);
        CustomerModel GetCustomerByPhone(string phone);
        void AddCustomer(CustomerModel unitCustomer);
        void UpdateCustomer(CustomerModel unitCustomer);
        void DeleteCustomer(int CustomerId);
    }
}
