﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeStoreSample.Core.DataAccessLayer.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        ICustomerRepo CustomerRepo { get; }
        void Commit();
    }
}