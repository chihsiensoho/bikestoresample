﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BikeStoreSample.Core.DataAccessLayer.Repo
{
    public abstract class RepoBase
    {
        //protected string connstr { get; private set; }
        //protected static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        protected SqlTransaction Transaction { get; private set; }
        protected SqlConnection Connection { get { return Transaction.Connection; } }

        public RepoBase()
        {
            //connstr = ConfigurationManager.ConnectionStrings["DBConnStr"].ConnectionString;
        }

        public RepoBase(SqlTransaction transaction)
        {
            Transaction = transaction;
        }
    }
}