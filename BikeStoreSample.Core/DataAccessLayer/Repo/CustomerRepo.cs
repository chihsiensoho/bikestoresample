﻿using BikeStoreSample.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using BikeStoreSample.Core.DataAccessLayer.Interface;
using Dapper.FluentMap;

namespace BikeStoreSample.Core.DataAccessLayer.Repo
{
    public class CustomerRepo : RepoBase , ICustomerRepo
    {
        public CustomerRepo(SqlTransaction transaction) : base(transaction)
        {
            FluentMapper.EntityMaps.Clear();
            FluentMapper.Initialize(cfg =>
            {
                cfg.AddMap(new CustomerModelMap());
            });
        }

        public CustomerModel GetCustomer(int CustomerId)
        {
            string sql = "SELECT [customer_id],[first_name],[last_name],[phone],[email],[street],[city],[state],[zip_code] FROM [sales].[customers] WHERE customer_id=@CustomerId";
            return Connection.QuerySingleOrDefault<CustomerModel>(sql, param: new { CustomerId }, transaction: Transaction);
        }


        public CustomerModel GetCustomerByPhone(string phone)
        {
            string sql = "SELECT [customer_id],[first_name],[last_name],[phone],[email],[street],[city],[state],[zip_code] FROM [sales].[customers] WHERE phone=@phone";
            return Connection.QuerySingleOrDefault<CustomerModel>(sql, param: new { phone }, transaction: Transaction);
        }

        public void AddCustomer(CustomerModel unitCustomer)
        {
            string columns = "first_name,last_name,phone,email,street,city,state,zip_code";
            string values = "@FirstName,@LastName,@Phone,@Email,@Street,@City,@State,@ZipCode";
            string sql = $"INSERT INTO  [sales].[customers]({columns}) VALUES({values})";
            Connection.Execute(sql, unitCustomer, transaction: Transaction);
        }

        public void UpdateCustomer(CustomerModel unitCustomer)
        {
            string sql = $"UPDATE [sales].[customers] SET " +
            @"first_name = @FirstName
            ,last_name = @LastName
            ,email = @Email
            ,street = @Street
            ,city = @City
            ,state = @State
            ,zip_code = @ZipCode           
            WHERE phone = @Phone";
            Connection.Execute(sql, unitCustomer, transaction: Transaction);
        }

        public void DeleteCustomer(int CustomerId)
        {
            string sql = $"DELETE FROM [sales].[customers] WHERE customer_id = @CustomerId";
            Connection.Execute(sql, param: new { CustomerId }, transaction: Transaction);
        }
    }
}