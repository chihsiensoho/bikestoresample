﻿using BikeStoreSample.Core.BusinessLayer.Interface;
using BikeStoreSample.Core.DataAccessLayer;
using BikeStoreSample.Core.DataAccessLayer.Interface;
using BikeStoreSample.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeStoreSample.Core.BusinessLayer
{
    public class CustomerService : ICustomerService
    {

        protected IUnitOfWork uow;

        public CustomerModel GetCustomer(int CustomerId)
        {
            using (uow = new UnitOfWork())
            {
                try
                {
                    CustomerModel unitCustomer = uow.CustomerRepo.GetCustomer(CustomerId);
                    return unitCustomer;

                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public void AddCustomer(CustomerModel unitCustomer)
        {
            using (uow = new UnitOfWork())
            {
                try
                {
                    uow.CustomerRepo.AddCustomer(unitCustomer);
                    uow.Commit();
                }
                catch (Exception ex)
                {
                }
            }
        }

        public void UpdateCustomer(CustomerModel unitCustomer)
        {
            using (uow = new UnitOfWork())
            {
                try
                {
                    uow.CustomerRepo.UpdateCustomer(unitCustomer);
                    uow.Commit();
                }
                catch (Exception ex)
                {
                }
            }
        }

        public void DeleteCustomer(string phone)
        {
            using (uow = new UnitOfWork())
            {
                try
                {
                    CustomerModel unit = uow.CustomerRepo.GetCustomerByPhone(phone);
                    if (unit != null)
                    {
                        uow.CustomerRepo.DeleteCustomer(unit.CustomerId);
                        uow.Commit();
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
}