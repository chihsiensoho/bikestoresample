﻿using BikeStoreSample.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeStoreSample.Core.BusinessLayer.Interface
{
    public interface ICustomerService
    {
        CustomerModel GetCustomer(int CustomerId);
        void AddCustomer(CustomerModel unitCustomer);
        void UpdateCustomer(CustomerModel unitCustomer);
        void DeleteCustomer(string phone);
    }
}