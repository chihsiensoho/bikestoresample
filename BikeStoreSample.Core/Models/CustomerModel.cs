﻿using Dapper.FluentMap.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BikeStoreSample.Core.Models
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }

    public class CustomerModelMap : EntityMap<CustomerModel>
    {
        public CustomerModelMap()
        {
            Map(x => x.CustomerId).ToColumn("customer_id", false);
            Map(x => x.FirstName).ToColumn("first_name", false);
            Map(x => x.LastName).ToColumn("last_name", false);
            Map(x => x.Phone).ToColumn("phone", false);
            Map(x => x.Email).ToColumn("email", false);
            Map(x => x.Street).ToColumn("street", false);
            Map(x => x.City).ToColumn("city", false);
            Map(x => x.State).ToColumn("state", false);
            Map(x => x.ZipCode).ToColumn("zip_code", false);
        }
    }
}