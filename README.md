# BikeStore Sample #



### 說明

此為實作Restful API CRUD的範例程式, 在資料的存取的部份, 以Repository模式與 UnitOfWork模式搭配Dapper使用, 

Dependency Injection 以Autofac實現



### 目錄配置

```
BikeStoreSample
|
└── BikeStoreSample.Core  
|	└─| BusinessLayer 
|	     └─ Interface 
|	  | DataAccessLayer
|	     └─ Interface 
|	     └─ Repo 
|	  | Models
|
└── BikeStoreSample.Api 
|	└──| Controllers
|
README.md

```



### 使用套件 

* Autofac
* Dapper
* Dapper.FluentMap
* Newtonsonft.Json





