﻿using BikeStoreSample.Core.BusinessLayer.Interface;
using BikeStoreSample.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BikeStoreSample.Api.Controllers
{
    public class CustomerController : BaseController
    {
        private ICustomerService CustomerService;

        public CustomerController(ICustomerService _CustomerService)
        {
            this.CustomerService = _CustomerService;
        }

        [Route("Api/Customer/Add")]
        [HttpPost]
        public IHttpActionResult Add(CustomerModel reqCustomer)
        {
            CustomerService.AddCustomer(reqCustomer);

            var resp = new
            {
                code = 0
                //data = unit
            };
            return Content(HttpStatusCode.OK, resp);
        }

        [Route("Api/Customer/Get")]
        [HttpGet]
        public IHttpActionResult Get(ReqCustomer req)
        {
            var unit = CustomerService.GetCustomer(req.CustomerId);

            var resp = new
            {
                code = 0,
                data  = unit
            };
            return Content(HttpStatusCode.OK, resp);
        }

        [Route("Api/Customer/Update")]
        [HttpPut]
        public IHttpActionResult Update(CustomerModel req)
        {
            CustomerService.UpdateCustomer(req);

            var resp = new
            {
                code = 0
            };
            return Content(HttpStatusCode.OK, resp);
        }

        [Route("Api/Customer/Delete")]
        [HttpDelete]
        public IHttpActionResult Delete(string phone)
        {
            CustomerService.DeleteCustomer(phone);

            var resp = new
            {
                code = 0
            };
            return Content(HttpStatusCode.OK, resp);
        }
    }
}
